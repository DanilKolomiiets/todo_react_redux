import React, { useState,useEffect } from 'react'
import './App.scss';

import { db } from './firebase';

import AppContent from './components/AppContent'

export default function App() {
const [todos, setTodos] = useState([]);

  useEffect(()=> {
    db.collection("todos")
      .get()
      .then(
        snapshot => {
          const todos = snapshot.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
          }));

          setTodos(todos);
      })
      .catch(error => {
          console.log("Error getting documents: ", error);
      });
  }, []);

  return (
      <div className="app">
        <header>
          <h1>To Do App</h1>
        </header>
        <AppContent>
          <ul>
            {todos.map(todo=>
                <li key={todo.id}>{todo.Title}</li>
              )}
          </ul>
        </AppContent>
        
      </div>
  );
}

